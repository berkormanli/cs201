#include <iostream>
#include <string>
#include "search_string.h"

using namespace std;

string stringToSearch;
string parameter;

void Searchstring::contains(string stringToSearch, string parameter)
{
	unsigned int index = 0, length;

	length = stringToSearch.length();
	while (index < length)		// to end the search at the end of your sentence
	{
		index = stringToSearch.find(parameter, index);		// this finds the index[search] starting from index

		if (index != string::npos)		// if index is valid
		{
			char space = ' ';

			unsigned int spacePos = stringToSearch.find(space,index);		// this part finds the spaces at the end and beginning of the word which includes search string
			unsigned int spacePosR = stringToSearch.rfind(space, index);

			string word = stringToSearch.substr(spacePosR+1,spacePos-spacePosR-1);		// this gets the word without spaces using substr(); function

			cout << "index: " << index << " word: "<<word<<endl;		// prints out index and word
			index = index + parameter.length();		// index increase by search length
		}
	}
}

void Searchstring::start_or_end(string stringToSearch, string parameter)
{
	unsigned int index = 0, length;

	length = stringToSearch.length();		// to end the search at the end of your sentence
	while (index < length)
	{
		index = stringToSearch.find(parameter, index);		// this finds the index[search] starting from index

		if (index != string::npos)		// if index is valid
		{
			char space = ' ';

			unsigned int spacePos = stringToSearch.find(space,index);		// this part finds the spaces at the end and beginning of the word which includes search string
			unsigned int spacePosR = stringToSearch.rfind(space, index);

			string word = stringToSearch.substr(spacePosR+1,spacePos-spacePosR-1);		// this gets the word without spaces using substr(); function

			if (word.at(0) == parameter.at(0) || word.at(word.length()-1) == parameter.at(parameter.length()-1))			// condition checks if the character(s) at the end or beginning of the word and search string are same or not, if they are same print out index and word
			{
				cout << "index: " << index << " word: "<<word<<endl;		// prints out index and word
			}

			index = index + word.length() - parameter.length() -1 ;		// index increase by search length
		}
	}
}

void Searchstring::at_the_end(string stringToSearch, string parameter)
{
	unsigned int index = 0, length;

	length = stringToSearch.length();		// to end the search at the end of your sentence
	while (index < length)
	{
		index = stringToSearch.find(parameter, index);		// this finds the index[search] starting from index

		if (index != string::npos)		// if index is valid
		{
			char space = ' ';

			unsigned int spacePos = stringToSearch.find(space,index);		// this part finds the spaces at the end and beginning of the word which includes search string
			unsigned int spacePosR = stringToSearch.rfind(space, index);

			string word = stringToSearch.substr(spacePosR+1,spacePos-spacePosR-1);		// this gets the word without spaces using substr(); function

			if (word.at(word.length()-1) == parameter.at(parameter.length()-1))			// condition checks if the character(s) at the end of the word and search string are same or not, if they are same print out index and word
			{
				cout << "index: " << index << " word: "<<word<<endl;		// prints out index and word
			}

			index = index + parameter.length();		// index increase by search length
		}
	}
}

void Searchstring::search_contains(string stringToSearch, string parameter, Searchstring search)
{
	int word_contains = parameter.find("*");		// stores the index of *
	int word_at_the_end = parameter.find(".");		// stores the index of .
	int word_start_or_end = parameter.find("+");		// stores the index +

	if (word_contains!=string::npos)		// checks if word_contains is valid
	{
		parameter = parameter.substr(0,word_contains);		// removes * from search string
		search.contains(stringToSearch, parameter);		// execute contains function
	}
	else if (word_at_the_end!=string::npos)		// checks if word_at_the_end is valid
	{
		parameter = parameter.substr(0,word_at_the_end);		// removes . from search string
		search.at_the_end(stringToSearch, parameter);		// execute at_the_end function
	}
	else if (word_start_or_end!=string::npos)		// checks if word_start_or_end is valid
	{
		parameter = parameter.substr(0,word_start_or_end);		// removes + from search string
		search.start_or_end(stringToSearch, parameter);		// execute start_or_end function
	}
}