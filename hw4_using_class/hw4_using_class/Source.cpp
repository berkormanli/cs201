#include <iostream>
#include <string>
#include "search_string.h"

using namespace std;

int main()
{
	string your_search;
	string your_sentence;
	string your_input_word;
	Searchstring search;

	cout << "This program finds you the character(s) that you want to search in a sentence and prints out the word that includes the character(s) with their indexes." << endl << endl;
	cout << "Your input can't have punctuation characters such as '.' & ','. Also your inputs are case sensitive. You should use '+' at the end of your search input to find out if there is any word that starts or ends with your search input. You should use '.' at the end of your search input to find out if there is any word that ends with your search input. You should use '*' at the end of your search input to find out if there is any word that includes your search input. Enter 'QUIT' or 'quit' to exit the program." << endl << endl;
	cout << "Please enter your sentence with 'END' at the end of the sentence: ";

	while (your_input_word != "END" && your_input_word != "end")		// loop until user enters "end" or "END"
	{
		cin >> your_input_word;
		your_sentence = your_sentence + " " + your_input_word;		// get your_sentence adding your_input_word at the end of your_sentence
	}

	your_sentence=your_sentence.substr(1,your_sentence.length()-5);		// get actual your_sentence

	while (your_search != "QUIT" && your_search != "quit")		// loop until user enters "quit" or "QUIT"
	{
		cout << "\nPlease enter the character(s) that you want to search: ";
		cin >> your_search;
		search.search_contains(your_sentence,your_search,search);		// execute search_contains function
	}
	return 0;
}