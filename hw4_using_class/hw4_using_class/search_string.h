#ifndef SEARCH_STRING_H
#define SEARCH_STRING_H

#include <string>
using namespace std;

class Searchstring
{
	string stringToSearch,parameter,your_search,your_input,your_sentence;
public:
	void contains(string stringToSearch, string parameter);
	void at_the_end(string stringToSearch, string parameter);
	void start_or_end(string stringToSearch, string parameter);
	void search_contains(string stringToSearch, string parameter, Searchstring search);
};

#endif SEARCH_STRING_H