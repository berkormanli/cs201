#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void good_bye(string customer_name, string coffee_name)
{
	cout << "Here is your " << coffee_name << " " << customer_name <<".";
	cout << "\nHave a great day!";
}

bool ask_for_ingredient(string ingredient)
{
	string customer_choice;
	string yes="yes";
	string no="no";
	cout << "Do you want to have " << ingredient << " in it?";
	cin >> customer_choice;
	if (customer_choice.compare(yes)==0)
	{
		return true;
	}
	else if (customer_choice.compare(no)==0)
	{
		return false;
	}
}

bool foam_and_milk_equal(string first_ingredient, string second_ingredient)
{
	string customer_choice;
	string yes="yes";
	string no="no";
	cout << "Do " << first_ingredient << " and " << second_ingredient << " are in equal amounts?";
	cin >> customer_choice;
	if (customer_choice.compare(yes)==0)
	{
		return true;
	}
	else if (customer_choice.compare(no)==0)
	{
		return false;
	}
}

void we_make_your_coffee(string customer_name)
{
	cout << "Dear " << customer_name << ", we would like to ask some questions to make your coffee.\n";
	bool is_user_want_espresso = ask_for_ingredient("espresso");
	if (is_user_want_espresso)
	{
		bool is_user_want_milk = ask_for_ingredient("milk");
		if (is_user_want_milk)
		{
			bool is_user_want_hot_chocolate = ask_for_ingredient("hot chocolate");
			if (is_user_want_hot_chocolate)
			{
				good_bye(customer_name,"mocha");
			}
			else{
				bool is_user_want_foam = ask_for_ingredient("foam");
				if (is_user_want_foam)
				{
					bool is_foam_and_milk_equal = foam_and_milk_equal("foam","milk");
					if (is_foam_and_milk_equal)
					{
						good_bye(customer_name,"cappuccino");
					}
					else
					{
						good_bye(customer_name,"latte");
					}
				}
				else
				{
					good_bye(customer_name,"au lait");
				}
			}
		}
		else
		{
			bool is_user_want_water = ask_for_ingredient("water");
			if (is_user_want_water)
			{
				good_bye(customer_name,"americano");
			}
			else
			{
				good_bye(customer_name,"espresso");
			}
		}
	}
	else
	{
		good_bye(customer_name,"filtered coffee");
	}
}

int main ()
{
	string customer_name;
	cout << "Welcome to YOUR coffeeshop, please enter your name to give an order: ";
	cin >> customer_name;

	we_make_your_coffee(customer_name);
	return 0;
}