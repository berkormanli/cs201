//Berk ORMANLI

#include <string>
#include "MiniFW_Modified.h"
#include "randgen.h"
#include "Robots_Modified.h"
#include "strutils.h"

using namespace std;

void thing_generator(int x1, int y1, int x2, int y2, int cellnumber)
{
	int x,y,num,i=0;
	RandGen randomnumber;
	while(i<cellnumber)		// to make it continue until 40 cells are picked
	{
		x = randomnumber.RandInt(x1, x2);		// random x coordinate
		y = randomnumber.RandInt(y1, y2);		// random y coordinate
		num = randomnumber.RandInt(1,10);		// random number for PutThings
		while(GetCellCount(x,y) != 0)
		{
			x = randomnumber.RandInt(x1, x2);		// this parts helps to
			y = randomnumber.RandInt(y1, y2);		// pick an empty cell
		}
		PutThings(x,y,num);
		i++;
	}
}

void turn_and_move(Robot& robot1,Robot& robot2,Direction direction1,Direction direction2)
{
	robot1.TurnFace(direction1);		// turns the robots to specified directions
	robot2.TurnFace(direction2);		// which is taken as parameters

	if (robot1.FacingWall()==true)					// checks if robot1 is faced wall after turning its direction
	{
		robot1.SetBump(robot1.GetBump()+1);			// robot1 shouldn't move if it faced wall so this increases its BumpCount by 1
	}
	if (robot2.FacingWall()==true)					// checks if robot2 is faced wall after turning its direction
	{
		robot2.SetBump(robot2.GetBump()+1);			// robot1 shouldn't move if it faced wall so this increases its BumpCount by 1
	}
	if (robot1.FacingWall()==false && robot2.FacingWall()==false)		// if any of our robots faces wall, both robot will move in their directions
	{
		robot1.Move();
		robot2.Move();
	}
}

int main()
{
	RandGen randomnumber;	 

	thing_generator(0,0,19,9,40);

	Robot player_robot(0,9), reflection_robot(19,9,west);		// creating player robot and its reflection robot
	reflection_robot.SetColor(red);		// setting reflection robots color
	player_robot.SetBump(0);		// These two lines sets
	reflection_robot.SetBump(0);	// the bump count to 0

	int key;		// declaring an integer called "key" to know which key is pressed


	while (GetThingCount(0,0,19,19) > 0 && (player_robot.is_alive() && reflection_robot.is_alive()))		// Loop until environment is empty or one/both robot(s) stalled.
	{	
		key = getchar();		// setting "key" value to the value of pressed key using getchar()

		if (IsPressed(keyRightArrow))		// controls which key is pressed
		{
			turn_and_move(player_robot,reflection_robot,east,west);
		}
		else if (IsPressed(keyLeftArrow))		// controls which key is pressed
		{
			turn_and_move(player_robot,reflection_robot,west,east);
		}
		else if (IsPressed(keyUpArrow))		// controls which key is pressed
		{
			turn_and_move(player_robot,reflection_robot,north,north);
		}
		else if (IsPressed(keyDownArrow))		// controls which key is pressed
		{
			turn_and_move(player_robot,reflection_robot,south,south);
		}
		while (GetCellCount(player_robot.GetXCoordinate(),player_robot.GetYCoordinate()) != 0 || GetCellCount(reflection_robot.GetXCoordinate(),reflection_robot.GetYCoordinate()) != 0)		// running PickThing() for both robots if anything left on the cell at either of robots coordinates
		{
			player_robot.PickThing();
			reflection_robot.PickThing();
		}
	}

	string message = "Player robot gathered ";																													// sets the message string and after that it adds the bag count and bump count informations to that
	message = message + itoa(player_robot.GetBagCount()) + " things and got stuck " + itoa(player_robot.GetBump()) + " times,\n"
		+ "Reflection robot gathered " + itoa(reflection_robot.GetBagCount()) + " things and got stuck " + itoa(reflection_robot.GetBump()) + " times.";

	ShowMessage (message);		// shows the message string

	return 0;
}
