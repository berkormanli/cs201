// Berk Ormanli
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "strutils.h"
using namespace std;

struct Movie
{
	string name;
	string director;
	double rating;
};

void up_the_case(string& str)
{
	bool should_upper = false;		
	for(int i=0;i<str.length();i++)
	{
		if(should_upper==false && (str.at(i)>='a' && str.at(i)<='z')  )//check if its a new word. 
			str.at(i)=str.at(i)+'A'-'a';

		if((str.at(i)>='a' && str.at(i)<='z') || (str.at(i)>='A' && str.at(i)<='Z') )//you can also change this condition to check for a space in the string
			should_upper=true;							//if(str.at(i)!=' ')		
		else 									//true... if character at i is an alphabet and false if not
			should_upper=false;							//it reiterates and changes the case of the next character depending on the condition
	}
}

void bubble_sort(vector<Movie>& a)
{
	bool should_swap = true;
	while(should_swap){
		should_swap = false;
		for (size_t i = 0; i < a.size()-1; i++) 
		{
			if (a[i].rating > a[i+1].rating )		//compare 2 vectors ratings and check if i's rating bigger than i+1's rating
			{
				swap(a[i],a[i+1]);					//swap i and i+1 vectors content
				should_swap = true;
			}
		}
	}
}

void open_file(string filename, ifstream& input, string type)
{
	cout << "Please enter a filename for " << type << " file: ";
	cin >> filename;
	input.open(filename.c_str());						//open the file
	if (input.fail())									//if cannot open the file
	{
		cout << "Can not find the specified file.";		//alert user
		open_file(filename, input, type);						//this line is for recursion
	}
}

Movie movie_director_rating(string str, ifstream& rating_input, ofstream& out)
{
	Movie movie = {};													//create an empty Movie struct
	string line, line2, rating_line;
	stringstream correcting(str);
	while( correcting >> str )											//add str to line to get the line without tabs and multiple spaces
	{
		line = line + " " + str;

	}
	int index = line.find(";");											//finds ";"
	movie.director = line.substr(1,index-1);							//substr director of the movie
	ToLower(movie.director);											//lowers all chars of director of the movie
	movie.name = line.substr(index+2, line.length()-index+1);			//substr name of movie
	ToLower(movie.name);												//lowers all chars of name of the movie
	line.clear();														//clear line string

	int k = 0;															//this will used to know how many rating is available at the rating file
	double rating = 0, temprating = 0;
	while (rating_input >> rating)										//gets rating of the movie first from rating_input
	{
		getline(rating_input,line2);									//gets the rest of the line to line2
		istringstream rating_line_get(line2);							//create a string stream from line2

		while(rating_line_get >> rating_line)							//if read from line2 successful
		{
			line = line + " " + rating_line;							//copy it to line string

		}
		string movie_of_line = line.substr(1, line.length()-1);			//substr name of the movie to movie_of_line
		ToLower(movie_of_line);											//lower movie_of_line string
		if(movie_of_line == movie.name)									//if movie.name and movie_of_line are same
		{
			rating = (temprating*k + rating)/(k+1);						//calculate the rating
			temprating = rating;										//
			movie.rating = rating;										//
			k++;
		}
		line.clear();													//clear line string
	}
	up_the_case(movie.director);										//upper the first char of movie.director
	ToUpper(movie.name);												//upper all chars of movie.name
	rating_input.clear();												//
	rating_input.seekg(0);												//clear the ifstream and return to the start of the file
	return movie;														//returns a movie struct!
}

int main()
{
	string director_file, rating_file, outfilename, line, s;
	ifstream director_input, rating_input;
	ofstream out;
	istringstream stringinput(director_file);
	vector<Movie> movie_list;																//create a vector using Movie struct

	open_file(director_file, director_input, "Movie-Director");
	open_file(rating_file, rating_input, "Rating");
	cout << "Enter the name of the output file: ";
	cin >> outfilename;
	out.open(outfilename);

	while (getline(director_input,director_file))											//if reading from director_input is successful
	{
		movie_list.push_back(movie_director_rating(director_file,  rating_input, out));		//call movie_director_rating and push_back its return to movie_list vector
	}
	bubble_sort(movie_list);																//when the reading is done, do a bubble sort
	for (int i = movie_list.size()-1; i >= 0; i--)											//start i from the end of vector
	{
		out << movie_list.size()-i << ", " << movie_list[i].name << ", " << movie_list[i].director << ", " << movie_list[i].rating << endl;			//prints out the information to the output file
	}
	out.close();																			//saves and closes the output file
	return 0;
}

