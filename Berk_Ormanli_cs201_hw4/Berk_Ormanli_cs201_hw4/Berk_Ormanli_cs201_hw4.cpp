﻿//Berk Ormanlı

#include <iostream>
#include <string>

using namespace std;

void contains(string stringToSearch, string paramater)
{
	unsigned int index = 0, length;

	length = stringToSearch.length();
	while (index < length)		// to end the search at the end of your sentence
	{
		index = stringToSearch.find(paramater, index);		// this finds the index[search] starting from index

		if (index != string::npos)		// if index is valid
		{
			char space = ' ';

			unsigned int spacePos = stringToSearch.find(space,index);		// this part finds the spaces at the end and beginning of the word which includes search string
			unsigned int spacePosR = stringToSearch.rfind(space, index);

			string word = stringToSearch.substr(spacePosR+1,spacePos-spacePosR-1);		// this gets the word without spaces using substr(); function

			cout << "index: " << index << " word: "<<word<<endl;		// prints out index and word
			index = index + paramater.length();		// index increase by search length
		}
	}
}

void at_the_end(string stringToSearch, string parameter)
{
	unsigned int index = 0, length;

	length = stringToSearch.length();		// to end the search at the end of your sentence
	while (index < length)
	{
		index = stringToSearch.find(parameter, index);		// this finds the index[search] starting from index

		if (index != string::npos)		// if index is valid
		{
			char space = ' ';

			unsigned int spacePos = stringToSearch.find(space,index);		// this part finds the spaces at the end and beginning of the word which includes search string
			unsigned int spacePosR = stringToSearch.rfind(space, index);

			string word = stringToSearch.substr(spacePosR+1,spacePos-spacePosR-1);		// this gets the word without spaces using substr(); function

			if (word.at(word.length()-1) == parameter.at(parameter.length()-1))			// condition checks if the character(s) at the end of the word and search string are same or not, if they are same print out index and word
			{
				cout << "index: " << index << " word: "<<word<<endl;		// prints out index and word
			}

			index = index + parameter.length();		// index increase by search length
		}
	}
}

void start_or_end(string stringToSearch, string parameter)
{
	unsigned int index = 0, length;

	length = stringToSearch.length();		// to end the search at the end of your sentence
	while (index < length)
	{
		index = stringToSearch.find(parameter, index);		// this finds the index[search] starting from index

		if (index != string::npos)		// if index is valid
		{
			char space = ' ';

			unsigned int spacePos = stringToSearch.find(space,index);		// this part finds the spaces at the end and beginning of the word which includes search string
			unsigned int spacePosR = stringToSearch.rfind(space, index);

			string word = stringToSearch.substr(spacePosR+1,spacePos-spacePosR-1);		// this gets the word without spaces using substr(); function

			if (word.at(0) == parameter.at(0) || word.at(word.length()-1) == parameter.at(parameter.length()-1))			// condition checks if the character(s) at the end or beginning of the word and search string are same or not, if they are same print out index and word
			{
				cout << "index: " << index << " word: "<<word<<endl;		// prints out index and word
			}

			index = index + parameter.length() + 1 ;		// index increase by search length
		}
	}
}

void search_contains(string stringToSearch,string parameter)
{

	int word_contains = parameter.find("*");		// stores the index of *
	int word_at_the_end = parameter.find(".");		// stores the index of .
	int word_start_or_end = parameter.find("+");		// stores the index +

	if (word_contains!=string::npos)		// checks if word_contains is valid
	{
		parameter = parameter.substr(0,word_contains);		// removes * from search string
		contains(stringToSearch, parameter);		// execute contains function
	}
	else if (word_at_the_end!=string::npos)		// checks if word_at_the_end is valid
	{
		parameter = parameter.substr(0,word_at_the_end);		// removes . from search string
		at_the_end(stringToSearch, parameter);		// execute at_the_end function
	}
	else if (word_start_or_end!=string::npos)		// checks if word_start_or_end is valid
	{
		parameter = parameter.substr(0,word_start_or_end);		// removes + from search string
		start_or_end(stringToSearch, parameter);		// execute start_or_end function
	}
}

int main(int argc, char* argv[])
{
	string your_search;
	string your_sentence;
	string your_input_word;

	cout << "This program finds you the character(s) that you want to search in a sentence and prints out the word that includes the character(s) with their indexes." << endl << endl;
	cout << "Your input can't have punctuation characters such as '.' & ','. Also your inputs are case sensitive. You should use '+' at the end of your search input to find out if there is any word that starts or ends with your search input. You should use '.' at the end of your search input to find out if there is any word that ends with your search input. You should use '*' at the end of your search input to find out if there is any word that includes your search input. Enter 'QUIT' or 'quit' to exit the program." << endl << endl;
	cout << "Please enter your sentence with 'END' at the end of the sentence: ";

	while (your_input_word != "END" && your_input_word != "end")		// loop until user enters "end" or "END"
	{
		cin >> your_input_word;
		your_sentence = your_sentence + " " + your_input_word;		// get your_sentence adding your_input_word at the end of your_sentence
	}

	your_sentence=your_sentence.substr(1,your_sentence.length()-5);		// get actual your_sentence

	while (your_search != "QUIT" && your_search != "quit")		// loop until user enters "quit" or "QUIT"
	{
		cout << "\nPlease enter the character(s) that you want to search: ";
		cin >> your_search;
		search_contains(your_sentence,your_search);		// execute search_contains function
	}
	return 0;
}
