#include <string>
#include "Robots.h"
#include <cmath>
#include <complex>

using namespace std;

void TurnBack(Robot& robot)
{
	robot.TurnRight();
	robot.TurnRight();
}

void TurnLeft(Robot& robot)
{
	robot.TurnRight();
	robot.TurnRight();
	robot.TurnRight();
}

void goRobotGo(Robot& robot, int x, int y, int xcoord, int ycoord)
{
	int distanceToTravelX = abs(x-xcoord);

		if (xcoord>x)
		{
			TurnBack(robot);
		}
		robot.Move(distanceToTravelX);

		int distanceToTravelY = abs(y-ycoord);


		if ((ycoord>y && robot.FacingEast()) || (ycoord<y && !robot.FacingEast()))
		{
			robot.TurnRight();
		}
		else if ((ycoord>y && !robot.FacingEast()) || (ycoord<y && robot.FacingEast()))
		{
			TurnLeft(robot);
		}

		robot.Move(distanceToTravelY);

		while (!robot.FacingEast())
		{
			robot.TurnRight();
		}
}

int distance(int x1,int x2,int y1, int y2)
{
	return abs(x1-x2)+abs(y1-y2);
}

int main ()
{
	int xcoord, ycoord;

	GetInput("Please enter the x coordinate", xcoord);
	GetInput("Please enter the y coordinate", ycoord);

	Robot myRobot(xcoord, ycoord);

	int target1 = distance(xcoord, 8, ycoord, 6);
	int target2 = distance(xcoord, 2, ycoord, 3);
	int target3 = distance(xcoord, 4, ycoord, 10);

	int min1 = min(target1,target2);
	min1 = min(min1, target3);

	if (min1 == target1)
	{
		
		goRobotGo(myRobot, 8,6,xcoord,ycoord);

		TurnBack(myRobot);
		myRobot.Move(4);
		myRobot.TurnRight();
		myRobot.Move(4);
		TurnBack(myRobot);
		myRobot.Move(7);
		myRobot.TurnRight();
		myRobot.Move(2);
		TurnBack(myRobot);
	}
	else
	{
		if (min1 == target2)
		{
			if (2-xcoord >= 0)
			{
				myRobot.Move(2-xcoord);
				TurnLeft(myRobot);
				if (3-ycoord >= 0)
				{
					myRobot.Move(3-ycoord);
					myRobot.TurnRight();
				}
				else
				{
					TurnBack(myRobot);
					myRobot.Move(abs(3-ycoord));
					TurnLeft(myRobot);
				}
			}
			else
			{
				TurnBack(myRobot);
				myRobot.Move(abs(2-xcoord));
				myRobot.TurnRight();
				if (3-ycoord >= 0)
				{
					myRobot.Move(3-ycoord);
					myRobot.TurnRight();
				}
				else
				{
					TurnBack(myRobot);
					myRobot.Move(abs(3-ycoord));
					TurnLeft(myRobot);
				}
			}

			myRobot.Move(2);
			TurnLeft(myRobot);
			myRobot.Move(7);
			myRobot.TurnRight();
			myRobot.Move(4);
			myRobot.TurnRight();
			myRobot.Move(4);
			TurnLeft(myRobot);

		}
		else
		{
			if (4-xcoord >= 0)
			{
				myRobot.Move(4-xcoord);
				TurnLeft(myRobot);
				if (3-ycoord >= 0)
				{
					myRobot.Move(10-ycoord);
					myRobot.TurnRight();
				}
				else
				{
					TurnBack(myRobot);
					myRobot.Move(abs(10-ycoord));
					TurnLeft(myRobot);
				}
			}
			else
			{
				TurnBack(myRobot);
				myRobot.Move(abs(4-xcoord));
				myRobot.TurnRight();
				if (10-ycoord >= 0)
				{
					myRobot.Move(10-ycoord);
					myRobot.TurnRight();
				}
				else
				{
					TurnBack(myRobot);
					myRobot.Move(abs(10-ycoord));
					TurnLeft(myRobot);
				}
			}

			myRobot.Move(4);
			myRobot.TurnRight();
			myRobot.Move(4);
			TurnLeft(myRobot);
			myRobot.TurnRight();
			myRobot.Move(3);
			myRobot.TurnRight();
			myRobot.Move(6);
			TurnBack(myRobot);

		}
	}
	return 0;
} 
