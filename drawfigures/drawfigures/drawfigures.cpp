#include <string>
#include "Robots.h"

using namespace std;

//Written by Albert Levi, November 9, 2003

// In this program robot starts at 0,0 and searches a rectangular space that covers n*n cells
// n is input. During this journey the robot should pick or put things on the cells 
// so that all visited cells occupy one thing 


void MakeCell1Thing (Robot & myrobot)
//pre: robot's bag contains at least one thing, if the current cell is empty
//post: after this function, robot's cell occupies one thing, no matter initial content is. 
//the remining things in the cell are transferred into the robot's bag
{
	while (!myrobot.CellEmpty())   //first make the cell empty
	{
	    myrobot.PickThing();
	}
	myrobot.PutThing();   //then put one thing
}

void TurnLeft (Robot & myrob)
//post: turns the robot left
{
	for (int i = 0; i < 3; i++)
  	    myrob.TurnRight();
}



int main()
{
	int i, j, n;

	GetInput("Please enter the length of one side of the rectangular area to be searched for", n);
	Robot rob(0,0,east,n*n);  //n*n: to make sure that there are enough things in the bag of robot

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n-1; j++)     //this loop is to move the robot on a straight line
			                          //the robot will visit n cells but since it is already on
					        		  //the first cell, it is going to Move() n-1 times
		{
			MakeCell1Thing(rob);   //at each iteration first make the cell's content 1 thing
			rob.Move();            //then move on the next cell
		}
				
		//at the end of each line, go up one cell, but before make the current cell's content 1
		MakeCell1Thing(rob);   

		if (i % 2 == 0)    //if went ----->
		{
			TurnLeft(rob);
			rob.Move();
			TurnLeft(rob);
		}
		else      // if went <------
		{
			rob.TurnRight();
			rob.Move();
			rob.TurnRight();
		}
	}

	return 0;
}
