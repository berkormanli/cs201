#include <iostream>
#include <conio.h>
using namespace std;

int main ()
{
	double unitPriceOfPicture;
	double unitPriceOfCake;
	double unitPriceOfSculpture;
	int amountOfPictureSold;
	int amountOfCakeSold;
	int amountOfSculptureSold;
	double totalMoneyGained;
	double moneyDonated;

	cout << "Please enter the unit price of pictures: ";
	cin >> unitPriceOfPicture;
	cout << "Please enter the unit price of cakes: ";
	cin >> unitPriceOfCake;
	cout << "Please enter the unit price of sculptures: ";
	cin >> unitPriceOfSculpture;
	cout << "Please enter the total number of pictures sold: ";
	cin >> amountOfPictureSold;
	cout << "Please enter the total number of cakes sold: ";
	cin >> amountOfCakeSold;
	cout << "Please enter the total number of sculptures sold: ";
	cin >> amountOfSculptureSold;

	totalMoneyGained=(unitPriceOfPicture*amountOfPictureSold)+(unitPriceOfCake*amountOfCakeSold)+(unitPriceOfSculpture*amountOfSculptureSold);
	moneyDonated=totalMoneyGained*0.85;

	cout << "Total money gained: " << totalMoneyGained;
	cout << ".\nAmount of money donated to the shelter: " <<moneyDonated;

	getch();
	return 0;
}
