﻿#include <iostream>
#include <fstream>
#include <string>
#include "date.h"
#include "strutils.h"
#include <sstream>
using namespace std;

unsigned check_month(unsigned& month, string which)
{
	string str;
	cout << "Please enter " << which << " date's month: ";
	cin >> str;
	stringstream(str) >> month;
	if(itoa(month) == str && (month >= 1 && month <= 12))
	{
		return month;
	}
	else
	{
		cout << "Integer value must be between 1 and 12. ";
		check_month(month,which);
	}
}

unsigned check_year(unsigned& year, string which)
{
	string str;
	cout << "Please enter " << which << " date's month: ";
	cin >> str;
	stringstream(str) >> year;
	if(itoa(year) == str && (year >= 1900 && year <= 2100))
	{
		return year;
	}
	else
	{
		cout << "Integer value must be between 1900 and 2100. ";
		check_month(year,which);
	}
}

void find_past(Date& d1, Date& d2)
{
	if(d1.Absolute() > d2.Absolute())					//if first entered date is after the second one
	{
		long diff = d1.Absolute()-d2.Absolute();		//finds the difference
		d1 = d1 - diff;									//this part makes first date second
		d2 = d2 + diff;									//and second date first
	}
}

void create_output(unsigned month1, unsigned month2, unsigned year1, unsigned year2, string& line, ofstream& out, istringstream& string_input, Date data, string gender)
{
	string outputname = gender;							//These lines create the output filename with
	outputname = outputname + "_"						//user-entered years and months
		+itoa(month1)+"-"+itoa(year1)+"_"
		+itoa(month2)+"-"+itoa(year2)+".txt";

	if (!out.is_open())									//if the ofstream isn't working
	{
		out.open(outputname);							//create and open the file
	}

	out << data.MonthName() << " " << data.Day()		//ofstream to the file in the format of "MonthName Day Year DayName"
		<< " " << data.Year() << " " << data.DayName();

	while(string_input>>line)							//if read successful
	{
		ToLower(line);									//lowers the string
		string firstchar = "";							//defines a string for the first char of the string
		firstchar = line.at(0);							//assign 0 index of the line string to our firstchar string
		ToUpper(firstchar);								//uppers the firstchar string
		line = firstchar + line.substr(1);				//defines line string with its first char upper and the others lower
		out << " " << line;								//outs the edited line string
	}
	out << endl;										//ends line
}

void replace_space(unsigned& index, string& line,string search)
{
	while (index < line.length())			//reads the entire string
	{
		index++;
		index = line.find(search);
		if(index != string::npos)			//if any search parameter found in string line
			line.replace(index,1," ");		//replace it with space
	}
	index = 0;
}

void open_file(string filename, ifstream& input)
{
	cout << "Enter the file name: ";
	cin >> filename;

	input.open(filename.c_str());						//open the file
	if (input.fail())									//if cannot open the file
	{
		cout << "Cannot open " << filename << " .";		//alert user
		open_file(filename, input);						//this line is for recursion
	}
}

int main()
{
	unsigned int index=0,month1,month2,year1,year2,bday,bmonth,byear;
	string filename,line,word;
	ifstream input;
	ofstream outM;
	ofstream outF;

	open_file(filename, input);

	check_month(month1, "first");
	check_year(year1,"first");
	check_month(month2, "second");
	check_year(year2,"second");

	Date date1(month1, 1, year1);
	Date date2(month2, 1, year2);

	find_past(date1, date2);

	while (getline(input,line))
	{
		replace_space(index, line,"/");

		replace_space(index, line,"\t");

		istringstream strinput(line);
		strinput >> bday >> bmonth >> byear;

		Date data(bmonth,bday,byear);

		if (data <= date2 && data >= date1)
		{
			strinput >> line;
			if (line == "m" || line == "M")
			{
				create_output(date1.Month(), date2.Month(), date1.Year(), date2.Year(), line, outM, strinput, data, "OutputMale");
			}
			else if (line == "f" || line == "F")
			{
				create_output(date1.Month(), date2.Month(), date1.Year(), date2.Year(), line, outF, strinput, data, "OutputFemale");
			}
			line.clear();
		}
	}
	outM.close();
	outF.close();
	return 0;
}